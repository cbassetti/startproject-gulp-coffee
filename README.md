# VIVO - Posicional

Landing page de posicionamento da marca VIVO, após fusão com GVT.

### Linguagens utilizadas:
  - HTML5
  - CSS (sass)
  - JS (coffeescript)

### Dependências:
  - Gulp
  - Compass
  - TweenMax
  - Jquery

### Versão
  - 1.0 (28/03/2016)

### Release notes

