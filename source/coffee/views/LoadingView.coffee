AView = require "./AView"
EventBroadcaster = require "../mars/event/EventBroadcaster"

class LoadingView extends AView

	initialize : (@options) ->
		super(@options)

	start: ->
		console.log "LoadingView start..."
		EventBroadcaster.on "update_percent_load", @update

	update:(val)->		
		$(".progress-txt").text parseInt(val) + '%'

	destroy:->
		super()
		

module.exports = LoadingView