Loader = require '../mars/loader/Loader.coffee'

class AView extends Backbone.View

	className: "view"
	templateVars: {}
	loader: Loader.getInstance()
	
	initialize : (@options) ->
		_.extend @, Backbone.Events
		@id = @options.data.id
		@template = @loader.get @options.data.template

	render: ->
		compiledTemplate = _.template(@template)(@templateVars)
		$(@el).append compiledTemplate
		@trigger "rendered-view", {type: "rendered-view", currentTarget: @}

	resize:=>
		

	destroy:->
		@trigger "destroyed-view", {type: "destroyed-view", currentTarget: @}

module.exports = AView