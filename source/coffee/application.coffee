Loader = require "./mars/loader/Loader"
EventBroadcaster = require "./mars/event/EventBroadcaster"
ViewsController = require "./mars/navigation/ViewsController"
DeviceDetector = require "./mars/device/DeviceDetector"
Router = require "./mars/navigation/Router"

class App

	views: [
		{
			"id":"loading",
			"template":"loading_template",
			"class": require "./views/LoadingView.coffee"
		},
		{
			"id":"home",
			"template":"home_template",
			"class": require "./views/HomeView.coffee"
		}
	]

	constructor:()->
		do @setUp

	setUp:=>
		mbDet = new DeviceDetector navigator.userAgent
		window.isMobile = mbDet.isMobile()
		
		@mobHeightFactor = if window.isMobile then 50 else 0

		window.W = $(window).width()
		window.H = $(window).height() - @mobHeightFactor

		$(window).resize @onResize

		@initLoader()

	initLoader:->
		@loader = Loader.getInstance()

		@loader.init({
			configURL:"./json/config.json"
			viewsURL:"./json/views.json"
		})

		@loader.on "onProgress", (progress) =>
			EventBroadcaster.trigger "update_percent_load", progress * 100

		@loader.on "preloadComplete", =>
			console.log "preload complete..."
			@preloadComplete()

		@loader.on "complete", =>
			console.log "load complete..."
			@loadComplete()

	preloadComplete:->
		@viewsController = ViewsController.getInstance()
		@viewsController.init @views, window.config.views
		@viewsController.on "created-view", @eventManager
		@viewsController.go "loading"
		@router = new Router viewsData:@views, viewsController:@viewsController

	loadComplete:=>
		@viewsController.go "home"


	setListeners:=>
		@viewsController?.off()
		@viewsController?.on "created-view", @eventManager
		@viewsController?.on "start-view", @eventManager
		@viewsController?._viewData?.view.off()
		@viewsController?._viewData?.view.on "rendered-view", @eventManager
		@viewsController?._viewData?.view.on "destroyed-view", @eventManager
		@viewsController?._viewData?.view.on "set-initial-point", @eventManager

	goToHome:()=>
		@viewsController.go "home"

	eventManager:(e)=>
		switch (e.type)
			when "created-view" then @setListeners()

			when "rendered-view" then @viewsController.manageViews()

			when "start-view" then @viewsController._viewData.view.start()

			when "navigate-view" then @viewsController.go e.currentTarget, e.params.content

			when "destroy-view" then console.log "destroy -view"

	onResize:=>
		window.W = $(window).width()
		window.H = $(window).height() - @mobHeightFactor
		@viewsController._viewData?.view?.resize()
	
init = ->
	app = new App()
	
document.addEventListener("DOMContentLoaded", init)