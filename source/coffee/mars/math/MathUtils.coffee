class MathUtils

	degToRad: (@deg) ->
		return @deg * (Math.PI / 180)

	radToDeg: (@rad) ->
		return @rad * (180 / Math.PI)

	getRandomInt:(@min, @max) ->
    	return Math.floor(Math.random() * (@max - @min + 1)) + @min;

    getRandomArbitrary:(@min, @max) ->
    	return Math.random() * (@max - @min) + @min;

	flattenTreemap: (rawtreemap)->
		flattreemap = []
		i = undefined
		j = undefined
		i = 0
		while i < rawtreemap.length
			j = 0
			while j < rawtreemap[i].length
				flattreemap.push rawtreemap[i][j]
				j++
			i++
		flattreemap

	improvesRatio: (currentrow, nextnode, length) =>
		newrow = undefined
		return true  if currentrow.length is 0
		newrow = currentrow.slice()
		newrow.push nextnode
		currentratio = @calculateRatio(currentrow, length)
		newratio = @calculateRatio(newrow, length)
		currentratio >= newratio

	calculateRatio: (row, length) =>
		min = Math.min.apply(Math, row)
		max = Math.max.apply(Math, row)
		sum = @sumArray(row)
		Math.max Math.pow(length, 2) * max / Math.pow(sum, 2), Math.pow(sum, 2) / (Math.pow(length, 2) * min)

	isArray: (arr) ->
		arr and arr.constructor is Array

	normalize: (data, area) ->
		normalizeddata = []
		sum = @sumArray(data)
		multiplier = area / sum
		i = undefined
		i = 0
		while i < data.length
			normalizeddata[i] = data[i] * multiplier
			i++
		normalizeddata


	sumArray: (arr) ->
		sum = 0
		i = undefined
		i = 0
		while i < arr.length
			sum += arr[i]
			i++
		sum

	sumMultidimensionalArray: (arr)->
		i = undefined
		total = 0
		if isArray(arr[0])
			i = 0
			while i < arr.length
				total += sumMultidimensionalArray(arr[i])
				i++
		else
			total = sumArray(arr)
		total

module.exports = MathUtils