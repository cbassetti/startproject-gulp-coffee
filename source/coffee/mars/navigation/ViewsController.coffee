class ViewsController
	@getInstance:(options)->
		@instance ?= new @(options)

	_viewsDictionary: {}
	_viewData:null
	_oldViewData:null
	_viewContainer:null
	_viewDataConfig:null
	_oldViewDataConfig:null

	constructor: (options) ->
		_.extend @, Backbone.Events

	init:(@_viewsData, @_views) =>
		for v in @_viewsData
			@_viewsDictionary[v.id] = v
		@_viewContainer = @_views.one
		return

	go: (id, content)->
		return if !@_viewsDictionary[id]? || (@_viewData?.id == id && !content?.allowRepeatView)
		@_viewData?.view?.destroy()
		@_viewData = @_viewsDictionary[id]
		@_oldViewData = @_viewData 
		
		@_content = if content != undefined then content else _.findWhere(window.views.data, {id: id})
		@_viewData.view = new @_viewData.class({data:@_viewData, content: @_content})
		@_viewData.view.render()

		@trigger "created-view", {type: "created-view", currentTarget: @_viewData.view}

		@_viewDataConfig = @_viewData?.view?.options?.content?.config
		@_oldViewDataConfig = @_viewDataConfig
		@manageViews()
		
	manageViews:=>
		@_oldViewContainer = @_viewContainer
		@_viewContainer = if @_viewContainer == @_views.one then @_views.two else @_views.one

		@setIndex(20, 10)

		clearTimeout @animOutTime

		if @_oldViewDataConfig and @_oldViewDataConfig.out_time
			$(@_oldViewContainer).find(".template").addClass @_oldViewDataConfig.out_anim
			
			@animOutTime = setTimeout =>
				@clearAll()
				@insertView()
			, @_oldViewDataConfig.out_time * 1000
		else
			@insertView()
			@clearAll()

	insertView:=>
		$(@_viewContainer).html(@_viewData.view.el)
		@trigger "start-view", {type: "start-view", currentTarget: @}

	setIndex:(currentIndex, oldIndex)=>
		$(@_viewContainer).css("z-index", currentIndex);
		$(@_oldViewContainer).css("z-index", oldIndex);

	clearAll:->
		@_content = null
		clearTimeout @clearTime
		clearTimeout @animOutTime
		$(@_oldViewContainer).empty()
		$(@_oldViewContainer).find(".template").remove()

module.exports = ViewsController