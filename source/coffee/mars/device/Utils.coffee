DevDetector = require './DeviceDetector.coffee'

class Utils 

	setFullScreenMode:->
		docElem = document.documentElement
		if docElem.requestFullscreen
			return docElem.requestFullscreen()
		else if docElem.webkitrequestFullscreen
			return docElem.webkitrequestFullscreen()

	preventUserMove:->
		$(document).bind "touchmove", (e)=>
			e.stopPropagation()
			e.preventDefault()

	preventSleep:->
		@device = new DevDetector(navigator.userAgent)

		if @device.isAndroid()
			$('#videoAudioLoop').remove()
			$("body").append("<video id='videoAudioLoop' width='1' height='1' loop><source src='assets/video/silence.mp4' type='video/mp4'></video>")
		else
			preventSleep = setInterval ->
				location.href = location.href
				window.setTimeout window.stop, 0
				return
			, 10000

	isVibrate:->
		navigator.vibrate = navigator.vibrate or navigator.webkitVibrate or navigator.mozVibrate or navigator.msVibrate
		if navigator.vibrate
			return true

	vibratePhone:(time)->
		navigator.vibrate time


	formatDate: (date) ->
		normalisedDate = new Date(date - (date.getTimezoneOffset() * 60 * 1000))
		normalisedDate = normalisedDate.toISOString().replace /\..+$|[^\d]/g, ''
		# removing seconds
		normalisedDate.substr 0, normalisedDate.length-2

module.exports = Utils