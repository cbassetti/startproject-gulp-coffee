#modulo do carousel com swipe
class Carousel
	constructor:(@settings)->
		@obj = $(@settings.target)
		@settings.currentIndex = @settings.initialIndex || 0
		@settings.len = @obj.find(".carousel-list li").length
		@settings.itemsPerPage = 1
		@settings.isItens = no
		@settings.callback = @settings.callback or undefined

		@build()
		@setSizes()
		

	build:=>
		
		swipeOptions = 
			triggerOnTouchEnd: true
			allowPageScroll: 'vertical'
			threshold: 75
			swipeStatus: @swipeStatus
		
		@obj.swipe swipeOptions

		if @obj.attr "time-interval"
			@settings.durationSlide = @obj.attr "duration-slide"
			@updateTimer()

		if @obj.attr "bullets"
			@buildBullets()
		
		if @obj.attr("arrows")
			if $(window).width() > 1023
				$(".arrows").css "display", "block"
			@buildArrows()

		# $(".item", @obj).bind "click", @redirectUrl

		if @obj.attr("item-per-page") and @obj.attr("item-per-page") > 1
			@settings.isItens = yes
			@settings.itemsPerPage = @obj.attr "item-per-page"

		$(window).resize @onResize
		@setSizes()

	redirectUrl:(e) =>
		return if @isInteraction
		path = $(e.currentTarget).attr "data-url"
		window.location.href = path;

	onResize: =>	
		@setSizes()
		@move @settings.containerW
				
	setSizes: =>
		if @settings.isItens
			item = @obj.find(".carousel-list li")
			if item.length > 0
				@obj.find(".carousel-container").addClass "more-items"
				@settings.containerW = @obj.find(".carousel-container").width()
				@settings.itemW = item.width() + (item.css("padding-left").substring(0, item.css("padding-left").length-2) * 2)
				@obj.find(".carousel-list").css "width", @settings.itemW * @settings.len
				@obj.find(".carousel-list").css "height", item.height()
		else	
			@settings.containerW = if window.isMobile then ($(window).width() - 60) else $(window).width()
			@obj.find(".carousel-list").css "width", (@settings.containerW * @settings.len)
			@obj.find(".carousel-list li").css "width", @settings.containerW
			@obj.css "height", @obj.find(".carousel-list li").height()

	swipeStatus: (event, phase, direction, distance) =>
		if event.type isnt "mouseup" or event.type isnt "mousedown"
			if (phase == 'move'and (direction == 'left' || direction == 'right'))
				window.clearTimeout @resetRedirect
				@isInteraction = true
				@clearTimer()
				duration = 0
				if (direction == 'left')
					@scrollImages((@settings.containerW * @settings.currentIndex) + distance, duration)
				else if (direction == 'right')
					@scrollImages((@settings.containerW * @settings.currentIndex) - distance, duration)
			else if (phase == 'cancel')
				@isInteraction = false
				@scrollImages(@settings.containerW * @settings.currentIndex, @settings.speed)
			else if (phase == 'end')
				if @obj.attr "time-interval"
					@resetTimer()
				if (direction == 'right')
					@prev()
				else if (direction == 'left')
					@next()
				@resetRedirect = setTimeout =>
					@isInteraction = false
				, 300

	prev: =>
		@settings.currentIndex = Math.max @settings.currentIndex - 1, 0
		if @settings.isItens
			@move @settings.itemW * @settings.itemsPerPage
		else
			@move @settings.containerW

	next: =>
		if @settings.isItens
			@settings.currentIndex = Math.min @settings.currentIndex + 1, @settings.len - @settings.itemsPerPage
			
			@move @settings.itemW * @settings.itemsPerPage
		else
			@settings.currentIndex = Math.min @settings.currentIndex + 1, @settings.len - 1
			@move @settings.containerW


	move: (diffW) =>
		@scrollImages diffW * @settings.currentIndex, @settings.speed
		if @settings.callback != undefined
			@settings.callback(@settings.currentIndex)

	scrollImages: (distance, duration) =>
		@obj.find(".carousel-list").css('transition-duration', (duration / 1000).toFixed(1) + 's')
		value = (if distance < 0 then '' else '-') + Math.abs(distance).toString()

		@obj.find(".carousel-list").css {
			'-moz-transform':"translate(#{value}px,0)"
			'-ms-transform':"translate(#{value}px,0)"
			'-webkit-transform':"translate(#{value}px,0)"
			'transform':"translate(#{value}px,0)"
		}

		if window.navigator.userAgent is "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Win64; x64; Trident/4.0; .NET CLR 2.0.50727; SLCC2; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; InfoPath.3)" or window.navigator.userAgent is "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)"
			@obj.find(".carousel-list").animate { "margin-left": "#{value}px" }, duration

		@updateArrows()
		@updateBullets()

	buildBullets: =>
		@obj.find(".bullets").remove()
		@obj.append "<ul class='bullets'></ul>"
			
		for i in [0..@settings.len-1]
			if i is 0 then className = "bullet selected" else className = "bullet"
			@obj.find(".bullets").append "<li id='bl-#{i}' class='#{className}'></li>"

		@obj.find(".bullet").bind "click", @bulletsControl

	buildArrows: =>
		@arrowL = $(@settings.arrowL)
		@arrowR = $(@settings.arrowR)
		@arrowL.bind "click", @arrowsControl
		@arrowR.bind "click", @arrowsControl
		
	arrowsControl: (e)=>
		e.preventDefault()
		if $(e.currentTarget).hasClass "right"
			@next()
		else
			@prev()
		@updateArrows()
		if @obj.attr "time-interval"
			@resetTimer()	

	updateArrows: =>
		if @arrowL != undefined
			@arrowL.css "opacity", 1
			@arrowR.css "opacity", 1
			targetArrow = null
			if @settings.currentIndex is @settings.len-1 or @settings.currentIndex is @settings.len - @settings.itemsPerPage
				targetArrow = @arrowR
			else if @settings.currentIndex is 0
				targetArrow = @arrowL

			if targetArrow isnt null
				targetArrow.css "opacity", 0.2	


	bulletsControl: (e) =>
		bt = e.currentTarget
		currentId = $(bt).attr("id").substring(3, 4)
		if @settings.currentIndex isnt $(bt).attr("id").substring(3, 4)
			@scrollImages(@settings.containerW * $(bt).attr("id").substring(3,4) , @settings.speed)
			@settings.currentIndex = currentId
			@updateBullets()
		if @obj.attr "time-interval"
			@resetTimer()
		
		if @settings.callback != undefined
			@settings.callback(@settings.currentIndex)

	updateBullets: =>
		@obj.find(".bullet").removeClass "selected"
		@obj.find("#bl-#{@settings.currentIndex}").addClass "selected"

	updateTimer: =>
		@settings.updateCount = window.setInterval =>
			if @settings.currentIndex is @settings.len-1
				@settings.currentIndex = -1
			@next()
		, @settings.durationSlide * 1000

	resetTimer: =>
		@clearTimer()
		@updateTimer()

	clearTimer: =>
		window.clearInterval @settings.updateCount

	destroy:=>
		console.log "destroy"
		@clearTimer()
		@obj.find(".bullets").remove()

module.exports = Carousel