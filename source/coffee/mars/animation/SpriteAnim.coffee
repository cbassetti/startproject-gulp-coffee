class SpriteAnim

	constructor: (w, h, frmW, frmH, totalFrames, speed) ->
		@obj = undefined
		@imageWidth = w or 1057
		@imageHeight = h or 34
		@frameWidth = frmW or 96
		@frameHeight = frmH or 34
		@totalFrames = totalFrames or 0
		@speed = speed or 60
		@timer = 0
		@curFrame =
		  x: 0
		  y: 0

	play:(obj) ->
		@obj = obj
		@curFrame =
		  x: 0
		  y: 0
		@loop()
		@timer = setInterval(@loop, 1000 / 45)
	
	loop: =>
		if @curFrame.x >= @totalFrames
			window.clearInterval @timer
			return
		xOffset = @frameWidth * @curFrame.x
		yOffset = @frameHeight * @curFrame.y
		@obj.style.backgroundPosition = -(xOffset) + "px " + -(yOffset) + "px"

		if (xOffset + @frameWidth) < @imageWidth
			@curFrame.x += 1
		else if (yOffset + @frameHeight) < @imageHeight
			@curFrame.x = 0
			@curFrame.y += 1
		else
			@curFrame.x = 0
			@curFrame.y = 0

	pause:=>
		window.clearInterval @timer

	resume:=>
		@loop()
	
module.exports = SpriteAnim