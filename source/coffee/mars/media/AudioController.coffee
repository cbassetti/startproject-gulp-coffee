Loader = require '../loader/Loader.coffee'

class AudioController

	loader: Loader.getInstance()
	oldTrack: undefined
	volume: 100

	next:(@trackName, @trackLen)=>
		@configTrack = {}
		audio = _.findWhere(window.medias, {id: @trackName}).media

		@configTrack =
			uri: audio.src
			length: @trackLen
			id: @trackName

		if @oldTrack?
			@fadeOut()

		@[@trackName] = new SeamlessLoop @configTrack
		@[@trackName].start @trackName
		@oldTrack = @[@trackName]

	fadeOut:=>

		@old = @oldTrack
		@oldVolume = @volume

		@outVolume = =>

			if @oldVolume > 0

				@interval = setTimeout =>

					@oldVolume = @oldVolume - 10

					@old.volume( @oldVolume / 100)

					@outVolume()

				, 50

			else
				clearTimeout @interval
				@old.stop()

		@outVolume()

	setVolume: (vol) =>
		# console.log 'setVolume >>>>>', vol / 100
		@old = @oldTrack
		@old.volume( vol / 100 )

	stop:=>
		clearTimeout @interval
		@oldTrack?.stop()

module.exports = AudioController