class VideoPlayer

	constructor: () ->

	setUp:(target, id) =>
		@id = id
		@player = @create target, @id
		@params = { "preload": "none", "autoplay": no }
		@[@id] = videojs @player, @params, =>
			# console.log "VideoPlayer created..."

	create:(target) =>
		@videoTag = $video = $(target)
		$video.attr "id", @id
		$video.attr "width", "100%"
		$video.attr "height", "100%"
		$video.attr "class", "video-js vjs-default-skin vjs-big-play-centered"
		@videoTag.css 
			"display": "none"

		$video[0]

	getObj:=>
		return @player

	play: =>
		# console.log '<<< play >>>'
		# console.log '>>>', @[@id]
		@[@id].play()

		setTimeout =>
			@videoTag.css 
				"display": "block"
		,0

	pause: =>
		@[@id].pause()

	fullScreen: =>
		@[@id].requestFullscreen()

	dispose: =>
		@[@id].dispose()

	mute: (muteVolume) =>
		@[@id].muted(muteVolume)

	jumpTo: (value) =>
		@[@id].currentTime value

	currentTime: =>
		@[@id].currentTime

	addVideoMetadataLoadedListener: (listener) =>
		@[@id].on 'loadedmetadata', =>
			listener {'duration': Math.ceil @[@id].duration() }

	addVideoEndedListener: (listener) =>
		@[@id].on 'ended', -> listener()

module.exports = VideoPlayer