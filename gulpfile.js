var gulp = require('gulp'),
	compass = require('gulp-compass'),
	browserSync = require('browser-sync'),
	del = require('del'),
	sitemap = require('gulp-sitemap'),
	uglify = require('gulp-uglify'),
	htmlmin = require('gulp-htmlmin'),
	concat = require('gulp-concat'),
	browserify = require('browserify'),
	watchify = require('watchify'),
	source = require('vinyl-source-stream'),
	notify = require('gulp-notify'),
	php = require('gulp-connect-php'),
	cssmin = require('gulp-cssmin'),
	coffeeify = require('coffeeify'),
	plumber = require('gulp-plumber');

var PATH = {
    app:
    {
        root: './source',
        source: {},
        public: {}
    },
	dist:
	{
	    root: './public'
	}
};

PATH.app.source = {
	js: PATH.app.root + '/js',
	coffee: PATH.app.root + '/coffee',
	libs: PATH.app.root + '/libs',
	sass: PATH.app.root + '/sass'
};

PATH.app.bin = {
	root: PATH.dist.root,
	css: PATH.dist.root + '/css',
	js: PATH.dist.root + '/js',
	libs: PATH.dist.root + '/js/libs',
	img: PATH.dist.root + '/img'
};

var MATCHES = {
	html:'/**/*.html',
	sass:'/**/*.scss',
	coffee:'/**/*.coffee',
	js:'/**/*.js',
	css:'/**/*.css'
}

var USE_COFFEESCRIPT = true;
// bundles formados pelo browserify
var BUNDLES = [
	'application'
]



/* ---------- ESTRUTURA DE DEV ---------- */
gulp.task('browserify', function(){

	for (var i = 0; i < BUNDLES.length; i++){

		var folders = BUNDLES[i].split('/');
		var filename,
			path = "/";

		if (folders.length == 0){
			return;
		}
		else if (folders.length == 1){
			filename = BUNDLES[i];
		}
		else{
			filename = folders.splice(folders.length-1, 1)[0];
			path += folders.join("/") + "/";
		}

		var filenameInput = filename + ".js";
		var filenameOutput = filenameInput;
		var srcInput = PATH.app.source.js;

		if (USE_COFFEESCRIPT){
			filenameInput = filename + ".coffee";
			srcInput = PATH.app.source.coffee;
		}

		var src = srcInput + path + filenameInput;
		var distFolder = PATH.app.bin.js + path;

		var bundler = watchify(browserify({
			cache: {},
			packageCache: {},
			entries:[src],
			extensions: ['.coffee']
		}))

		var bundle = function(){
			return bundler
				.bundle()
				.on('error', handleErrors)
				.pipe(plumber())
				.pipe(source(filenameOutput))
				.pipe(gulp.dest(distFolder))
		}

		bundler.on('update', bundle)
		var stream = bundle()
		// reload after last bundle
		if (i == BUNDLES.length-1)
			stream.pipe(browserSync.reload({stream: true}));
}
});

gulp.task('concatLibs', function(){
	gulp.src([PATH.app.source.libs + MATCHES.js], {base: PATH.app.source.libs})
		.pipe(concat('application_libs.js'))
		.on('error', handleErrors)
		.pipe(gulp.dest(PATH.app.bin.js))
		.pipe(browserSync.reload({stream: true}));
})

gulp.task('compass', function (){
	return gulp.src(PATH.app.source.sass + MATCHES.sass)
        .pipe(compass({
			sass: PATH.app.source.sass,
			css: PATH.app.bin.css,
			image: PATH.app.bin.img
		}))
		.on('error', handleErrors)
        .pipe(gulp.dest(PATH.app.bin.css))
		.pipe(browserSync.reload({stream: true}));
});


gulp.task('watch', function (){
	gulp.watch(PATH.app.source.sass + MATCHES.sass, ['compass']);
	gulp.watch(PATH.app.source.js + MATCHES.js, ['browserify']);
	gulp.watch(PATH.app.source.coffee + MATCHES.coffee, ['browserify']);
	gulp.watch(PATH.app.source.libs + MATCHES.js, ['concatLibs','browserify']);
	gulp.watch(PATH.app.bin.modules + MATCHES.html, ['reload']);
	gulp.watch(PATH.app.bin.root + MATCHES.php, ['reload']);
});



gulp.task('reload', function (){
	browserSync.reload();
});

gulp.task('browser-sync', ['concatLibs', 'browserify', 'compass'], function(){
	browserSync({
		browser: 'chrome',
		open: false,
		port: process.env.PORT || 3000,
		logPrefix : 'gulp-coffee-sass-project',
		notify: true, // Notificação de popup no Browser para saber se houve um reload.
		server: {
			baseDir: "./public"
		}
	});
});

//task default
gulp.task('default',  ['browser-sync', 'watch']);

function handleErrors (){
	var args = Array.prototype.slice.call(arguments);

	notify.onError({
		title: "Compile Error",
		message: "<%= error.message %>"
	}).apply(this, args);

	this.emit('end');
}

//////////////////////// PROD //////////////////////
// Minificar CSS
gulp.task("minifyCss", function() {
	return gulp.src(PATH.app.bin.css + "**/*.css")
		.pipe(cssmin({
			showLog: true
		}))
		.pipe(gulp.dest(PATH.dist.root));
});

// Minificar Scripts
gulp.task("minifyJs", function() {
	return gulp.src(PATH.app.bin.js + "**/*.js")
		.pipe(uglify())
		.pipe(gulp.dest(PATH.dist.root));
});

// task prod
gulp.task('prod',  ['minifyCss','minifyJs']);
