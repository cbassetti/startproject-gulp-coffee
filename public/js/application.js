(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var App, DeviceDetector, EventBroadcaster, Loader, Router, ViewsController, init,
  bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

Loader = require("./mars/loader/Loader");

EventBroadcaster = require("./mars/event/EventBroadcaster");

ViewsController = require("./mars/navigation/ViewsController");

DeviceDetector = require("./mars/device/DeviceDetector");

Router = require("./mars/navigation/Router");

App = (function() {
  App.prototype.views = [
    {
      "id": "loading",
      "template": "loading_template",
      "class": require("./views/LoadingView.coffee")
    }, {
      "id": "home",
      "template": "home_template",
      "class": require("./views/HomeView.coffee")
    }
  ];

  function App() {
    this.onResize = bind(this.onResize, this);
    this.eventManager = bind(this.eventManager, this);
    this.goToHome = bind(this.goToHome, this);
    this.setListeners = bind(this.setListeners, this);
    this.loadComplete = bind(this.loadComplete, this);
    this.setUp = bind(this.setUp, this);
    this.setUp();
  }

  App.prototype.setUp = function() {
    var mbDet;
    mbDet = new DeviceDetector(navigator.userAgent);
    window.isMobile = mbDet.isMobile();
    this.mobHeightFactor = window.isMobile ? 50 : 0;
    window.W = $(window).width();
    window.H = $(window).height() - this.mobHeightFactor;
    $(window).resize(this.onResize);
    return this.initLoader();
  };

  App.prototype.initLoader = function() {
    this.loader = Loader.getInstance();
    this.loader.init({
      configURL: "./json/config.json",
      viewsURL: "./json/views.json"
    });
    this.loader.on("onProgress", (function(_this) {
      return function(progress) {
        return EventBroadcaster.trigger("update_percent_load", progress * 100);
      };
    })(this));
    this.loader.on("preloadComplete", (function(_this) {
      return function() {
        console.log("preload complete...");
        return _this.preloadComplete();
      };
    })(this));
    return this.loader.on("complete", (function(_this) {
      return function() {
        console.log("load complete...");
        return _this.loadComplete();
      };
    })(this));
  };

  App.prototype.preloadComplete = function() {
    this.viewsController = ViewsController.getInstance();
    this.viewsController.init(this.views, window.config.views);
    this.viewsController.on("created-view", this.eventManager);
    this.viewsController.go("loading");
    return this.router = new Router({
      viewsData: this.views,
      viewsController: this.viewsController
    });
  };

  App.prototype.loadComplete = function() {
    return this.viewsController.go("home");
  };

  App.prototype.setListeners = function() {
    var ref, ref1, ref10, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9;
    if ((ref = this.viewsController) != null) {
      ref.off();
    }
    if ((ref1 = this.viewsController) != null) {
      ref1.on("created-view", this.eventManager);
    }
    if ((ref2 = this.viewsController) != null) {
      ref2.on("start-view", this.eventManager);
    }
    if ((ref3 = this.viewsController) != null) {
      if ((ref4 = ref3._viewData) != null) {
        ref4.view.off();
      }
    }
    if ((ref5 = this.viewsController) != null) {
      if ((ref6 = ref5._viewData) != null) {
        ref6.view.on("rendered-view", this.eventManager);
      }
    }
    if ((ref7 = this.viewsController) != null) {
      if ((ref8 = ref7._viewData) != null) {
        ref8.view.on("destroyed-view", this.eventManager);
      }
    }
    return (ref9 = this.viewsController) != null ? (ref10 = ref9._viewData) != null ? ref10.view.on("set-initial-point", this.eventManager) : void 0 : void 0;
  };

  App.prototype.goToHome = function() {
    return this.viewsController.go("home");
  };

  App.prototype.eventManager = function(e) {
    switch (e.type) {
      case "created-view":
        return this.setListeners();
      case "rendered-view":
        return this.viewsController.manageViews();
      case "start-view":
        return this.viewsController._viewData.view.start();
      case "navigate-view":
        return this.viewsController.go(e.currentTarget, e.params.content);
      case "destroy-view":
        return console.log("destroy -view");
    }
  };

  App.prototype.onResize = function() {
    var ref, ref1;
    window.W = $(window).width();
    window.H = $(window).height() - this.mobHeightFactor;
    return (ref = this.viewsController._viewData) != null ? (ref1 = ref.view) != null ? ref1.resize() : void 0 : void 0;
  };

  return App;

})();

init = function() {
  var app;
  return app = new App();
};

document.addEventListener("DOMContentLoaded", init);


},{"./mars/device/DeviceDetector":2,"./mars/event/EventBroadcaster":3,"./mars/loader/Loader":4,"./mars/navigation/Router":5,"./mars/navigation/ViewsController":6,"./views/HomeView.coffee":8,"./views/LoadingView.coffee":9}],2:[function(require,module,exports){
var DeviceDetector;

DeviceDetector = (function() {
  function DeviceDetector(userAgent) {
    this.mobileDetect = new MobileDetect(userAgent);
  }

  DeviceDetector.prototype.mobile = function() {
    return this.mobileDetect.mobile();
  };

  DeviceDetector.prototype.isMobile = function() {
    return this.mobileDetect.is(this.mobileDetect.os());
  };

  DeviceDetector.prototype.isAndroid = function() {
    return this.mobileDetect.os() === "AndroidOS";
  };

  DeviceDetector.prototype.isIOS = function() {
    return this.mobileDetect.os() === "iOS";
  };

  DeviceDetector.prototype.isTablet = function() {
    return this.mobileDetect.tablet() != null;
  };

  return DeviceDetector;

})();

module.exports = DeviceDetector;


},{}],3:[function(require,module,exports){
var EventBroadcaster;

EventBroadcaster = {};

_.extend(EventBroadcaster, Backbone.Events);

module.exports = EventBroadcaster;


},{}],4:[function(require,module,exports){
var EventBroadcaster, Loader,
  bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

EventBroadcaster = require('../event/EventBroadcaster');

Loader = (function() {
  Loader.getInstance = function() {
    return this.instance != null ? this.instance : this.instance = new this();
  };

  Loader.prototype.imageSequences = {};

  function Loader() {
    this.parseSequenceAssets = bind(this.parseSequenceAssets, this);
    this.get = bind(this.get, this);
    this.onMediaProgressUpdate = bind(this.onMediaProgressUpdate, this);
    this.onProgress = bind(this.onProgress, this);
    this.onAssetsComplete = bind(this.onAssetsComplete, this);
    this.onAssetsError = bind(this.onAssetsError, this);
    this.onPreAssetsComplete = bind(this.onPreAssetsComplete, this);
    _.extend(this, Backbone.Events);
  }

  Loader.prototype.init = function(options) {
    this.options = options;
    return this.loadConfig();
  };

  Loader.prototype.loadConfig = function() {
    this.loaderConfig = new createjs.LoadQueue(true);
    this.loaderConfig.loadManifest([
      {
        id: 'config',
        src: this.options.configURL,
        type: createjs.LoadQueue.JSON
      }, {
        id: 'views',
        src: this.options.viewsURL,
        type: createjs.LoadQueue.JSON
      }
    ]);
    return this.loaderConfig.on("complete", (function(_this) {
      return function(e) {
        window.config = _this.loaderConfig.getResult('config');
        window.config.preload_assets = _this.parseSequenceAssets(window.config.preload_assets);
        window.config.assets = _this.parseSequenceAssets(window.config.assets);
        window.views = _this.loaderConfig.getResult('views');
        return _this.preloadAssets();
      };
    })(this));
  };

  Loader.prototype.preloadAssets = function() {
    this.loaderPreAssets = new createjs.LoadQueue(true);
    this.loaderPreAssets.loadManifest(window.config.preload_assets);
    return this.loaderPreAssets.on("complete", this.onPreAssetsComplete);
  };

  Loader.prototype.onPreAssetsComplete = function() {
    this.trigger('preloadComplete');
    return this.loadAssets();
  };

  Loader.prototype.loadAssets = function() {
    this.loaderAssets = new createjs.LoadQueue(true);
    this.loaderAssets.on("progress", this.onProgress);
    this.loaderAssets.on("complete", this.onAssetsComplete);
    this.loaderAssets.on("error", this.onAssetsError);
    return this.loaderAssets.loadManifest(window.config.assets);
  };

  Loader.prototype.onAssetsError = function(data) {
    return console.log("" + data);
  };

  Loader.prototype.onAssetsComplete = function() {
    if (window.config.media.length > 0) {
      return this.loadMedia();
    } else {
      return this.trigger('complete');
    }
  };

  Loader.prototype.onProgress = function() {
    return this.trigger('onProgress', this.loaderAssets.progress);
  };

  Loader.prototype.onMediaProgressUpdate = function() {
    var j, len, percent, ref, total;
    total = 0;
    ref = this.arrPerc;
    for (j = 0, len = ref.length; j < len; j++) {
      percent = ref[j];
      total += percent.perc;
    }
    return EventBroadcaster.trigger('onMediaProgress', total / this.arrPerc.length);
  };

  Loader.prototype.get = function(id) {
    var result, result2;
    result = this.loaderPreAssets.getResult(id);
    if (result != null) {
      return result;
    }
    result2 = this.loaderAssets.getResult(id);
    return result2;
  };

  Loader.prototype.parseSequenceAssets = function(assets) {
    var digit, i, iImageEnd, iImageStart, iRangeEnd, iRangeStart, imageId, j, k, len, newAssets, newNode, newSrc, node, range, rangeStr, ref, ref1;
    newAssets = [];
    for (j = 0, len = assets.length; j < len; j++) {
      node = assets[j];
      if (node.type === "image_sequence") {
        this.imageSequences[node.id] = [];
        iRangeStart = node.src.indexOf("[") + 1;
        iRangeEnd = node.src.indexOf("]");
        range = node.src.substring(iRangeStart, iRangeEnd);
        rangeStr = "[" + range + "]";
        range = range.split("-");
        iImageStart = parseInt(range[0]);
        iImageEnd = parseInt(range[1]);
        for (i = k = ref = iImageStart, ref1 = iImageEnd; ref <= ref1 ? k <= ref1 : k >= ref1; i = ref <= ref1 ? ++k : --k) {
          digit = node.zeroPad != null ? this.zeroPad(i, node.zeroPad) : i;
          newSrc = node.src.replace(rangeStr, digit);
          imageId = node.id + "-" + i;
          newNode = {
            "id": imageId,
            "src": newSrc,
            "type": "image"
          };
          newAssets.push(newNode);
          this.imageSequences[node.id].push(imageId);
        }
      } else {
        newAssets.push(node);
      }
    }
    return newAssets;
  };

  return Loader;

})();

module.exports = Loader;


},{"../event/EventBroadcaster":3}],5:[function(require,module,exports){
var Router,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

Router = (function(superClass) {
  extend(Router, superClass);

  function Router() {
    return Router.__super__.constructor.apply(this, arguments);
  }

  Router.prototype.routes = {
    '': 'index'
  };

  Router.prototype.current = null;

  Router.prototype.initialize = function(options) {
    var i, len, ref, v;
    this.viewsController = options.viewsController;
    ref = options.viewsData;
    for (i = 0, len = ref.length; i < len; i++) {
      v = ref[i];
      this.add(v.id);
    }
    return Backbone.history.start();
  };

  Router.prototype.add = function(route) {
    route = ":" + route;
    this.routes[route] = "changeView";
    return this.route(route, "changeView");
  };

  Router.prototype.index = function() {
    var viewDefault;
    viewDefault = this.viewsController._viewsData[0].id;
    return this.changeView(viewDefault);
  };

  Router.prototype.changeView = function(id) {
    var route, routeFound;
    console.log("changeView", window.loadedAll);
    if (window.loadedAll) {
      for (route in this.routes) {
        route = route.substring(1, route.length);
        if (route === id) {
          routeFound = route;
          break;
        }
      }
      if ((routeFound != null) && routeFound !== this.current) {
        this.current = routeFound;
        return this.viewsController.go(routeFound);
      } else {
        return window.location.hash = "/home";
      }
    }
  };

  return Router;

})(Backbone.Router);

module.exports = Router;


},{}],6:[function(require,module,exports){
var ViewsController,
  bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

ViewsController = (function() {
  ViewsController.getInstance = function(options) {
    return this.instance != null ? this.instance : this.instance = new this(options);
  };

  ViewsController.prototype._viewsDictionary = {};

  ViewsController.prototype._viewData = null;

  ViewsController.prototype._oldViewData = null;

  ViewsController.prototype._viewContainer = null;

  ViewsController.prototype._viewDataConfig = null;

  ViewsController.prototype._oldViewDataConfig = null;

  function ViewsController(options) {
    this.setIndex = bind(this.setIndex, this);
    this.insertView = bind(this.insertView, this);
    this.manageViews = bind(this.manageViews, this);
    this.init = bind(this.init, this);
    _.extend(this, Backbone.Events);
  }

  ViewsController.prototype.init = function(_viewsData, _views) {
    var i, len, ref, v;
    this._viewsData = _viewsData;
    this._views = _views;
    ref = this._viewsData;
    for (i = 0, len = ref.length; i < len; i++) {
      v = ref[i];
      this._viewsDictionary[v.id] = v;
    }
    this._viewContainer = this._views.one;
  };

  ViewsController.prototype.go = function(id, content) {
    var ref, ref1, ref2, ref3, ref4, ref5, ref6;
    if ((this._viewsDictionary[id] == null) || (((ref = this._viewData) != null ? ref.id : void 0) === id && !(content != null ? content.allowRepeatView : void 0))) {
      return;
    }
    if ((ref1 = this._viewData) != null) {
      if ((ref2 = ref1.view) != null) {
        ref2.destroy();
      }
    }
    this._viewData = this._viewsDictionary[id];
    this._oldViewData = this._viewData;
    this._content = content !== void 0 ? content : _.findWhere(window.views.data, {
      id: id
    });
    this._viewData.view = new this._viewData["class"]({
      data: this._viewData,
      content: this._content
    });
    this._viewData.view.render();
    this.trigger("created-view", {
      type: "created-view",
      currentTarget: this._viewData.view
    });
    this._viewDataConfig = (ref3 = this._viewData) != null ? (ref4 = ref3.view) != null ? (ref5 = ref4.options) != null ? (ref6 = ref5.content) != null ? ref6.config : void 0 : void 0 : void 0 : void 0;
    this._oldViewDataConfig = this._viewDataConfig;
    return this.manageViews();
  };

  ViewsController.prototype.manageViews = function() {
    this._oldViewContainer = this._viewContainer;
    this._viewContainer = this._viewContainer === this._views.one ? this._views.two : this._views.one;
    this.setIndex(20, 10);
    clearTimeout(this.animOutTime);
    if (this._oldViewDataConfig && this._oldViewDataConfig.out_time) {
      $(this._oldViewContainer).find(".template").addClass(this._oldViewDataConfig.out_anim);
      return this.animOutTime = setTimeout((function(_this) {
        return function() {
          _this.clearAll();
          return _this.insertView();
        };
      })(this), this._oldViewDataConfig.out_time * 1000);
    } else {
      this.insertView();
      return this.clearAll();
    }
  };

  ViewsController.prototype.insertView = function() {
    $(this._viewContainer).html(this._viewData.view.el);
    return this.trigger("start-view", {
      type: "start-view",
      currentTarget: this
    });
  };

  ViewsController.prototype.setIndex = function(currentIndex, oldIndex) {
    $(this._viewContainer).css("z-index", currentIndex);
    return $(this._oldViewContainer).css("z-index", oldIndex);
  };

  ViewsController.prototype.clearAll = function() {
    this._content = null;
    clearTimeout(this.clearTime);
    clearTimeout(this.animOutTime);
    $(this._oldViewContainer).empty();
    return $(this._oldViewContainer).find(".template").remove();
  };

  return ViewsController;

})();

module.exports = ViewsController;


},{}],7:[function(require,module,exports){
var AView, Loader,
  bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

Loader = require('../mars/loader/Loader.coffee');

AView = (function(superClass) {
  extend(AView, superClass);

  function AView() {
    this.resize = bind(this.resize, this);
    return AView.__super__.constructor.apply(this, arguments);
  }

  AView.prototype.className = "view";

  AView.prototype.templateVars = {};

  AView.prototype.loader = Loader.getInstance();

  AView.prototype.initialize = function(options) {
    this.options = options;
    _.extend(this, Backbone.Events);
    this.id = this.options.data.id;
    return this.template = this.loader.get(this.options.data.template);
  };

  AView.prototype.render = function() {
    var compiledTemplate;
    compiledTemplate = _.template(this.template)(this.templateVars);
    $(this.el).append(compiledTemplate);
    return this.trigger("rendered-view", {
      type: "rendered-view",
      currentTarget: this
    });
  };

  AView.prototype.resize = function() {};

  AView.prototype.destroy = function() {
    return this.trigger("destroyed-view", {
      type: "destroyed-view",
      currentTarget: this
    });
  };

  return AView;

})(Backbone.View);

module.exports = AView;


},{"../mars/loader/Loader.coffee":4}],8:[function(require,module,exports){
var AView, HomeView,
  bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

AView = require("./AView");

HomeView = (function(superClass) {
  extend(HomeView, superClass);

  function HomeView() {
    this.destroy = bind(this.destroy, this);
    this.resize = bind(this.resize, this);
    this.start = bind(this.start, this);
    return HomeView.__super__.constructor.apply(this, arguments);
  }

  HomeView.prototype.initialize = function(options) {
    this.options = options;
    return HomeView.__super__.initialize.call(this, this.options);
  };

  HomeView.prototype.start = function() {
    return console.log("HomeView start...");
  };

  HomeView.prototype.resize = function() {};

  HomeView.prototype.destroy = function() {
    return HomeView.__super__.destroy.call(this);
  };

  return HomeView;

})(AView);

module.exports = HomeView;


},{"./AView":7}],9:[function(require,module,exports){
var AView, EventBroadcaster, LoadingView,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

AView = require("./AView");

EventBroadcaster = require("../mars/event/EventBroadcaster");

LoadingView = (function(superClass) {
  extend(LoadingView, superClass);

  function LoadingView() {
    return LoadingView.__super__.constructor.apply(this, arguments);
  }

  LoadingView.prototype.initialize = function(options) {
    this.options = options;
    return LoadingView.__super__.initialize.call(this, this.options);
  };

  LoadingView.prototype.start = function() {
    console.log("LoadingView start...");
    return EventBroadcaster.on("update_percent_load", this.update);
  };

  LoadingView.prototype.update = function(val) {
    return $(".progress-txt").text(parseInt(val) + '%');
  };

  LoadingView.prototype.destroy = function() {
    return LoadingView.__super__.destroy.call(this);
  };

  return LoadingView;

})(AView);

module.exports = LoadingView;


},{"../mars/event/EventBroadcaster":3,"./AView":7}]},{},[1]);
